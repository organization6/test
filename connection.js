const mongoose = require("mongoose");
const db = mongoose.connection;
const uri = "mongodb://localhost:27017/estacionart";

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

db.on("error", (err) => {
  console.log(err);
});

db.once("open", () => {
  console.log("we're connected to", uri);
});
