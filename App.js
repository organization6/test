//import React, { useState } from "react";
//import { StatusBar } from "expo-status-bar";
//import { StyleSheet, Text, View, Button, Platform } from "react-native";

require("./connection");

const User = require("./models/User");

const yenien2 = new User({
  name: "Yenien",
  lastname: "Rey",
  picture: "http://una.foto.piola.com.ar",
  age: 33,
  type: "Cliente",
  card: 4826,
  cellphone: 23861165,
  username: "Yenien",
  password: "Terrible password",
  creation_date: new Date(),
  rating: 8,
  acknowledgments: {
    amable: 50,
    "re fachero": 20,
  },
  badges: ["2500 reservas con exito", "2 años con EstacionarT"],
});

yenien2.save((err, doc) => {
  if (err) console.log(err);
  console.log(doc);
});

/* 
const Cat = (props) => {
  const [isHungry, setIsHungry] = useState(true);
  return (
    <View>
      <Text>
        I am {props.name}, and I am {isHungry ? "hungry" : "full"}!
      </Text>
      <Button
        onPress={() => {
          setIsHungry(false);
        }}
        disabled={!isHungry}
        title={isHungry ? "Pour me some milk, please!" : "Thank you!"}
      />
    </View>
  );
};

export default function App() {
  return (
    <View style={styles.container}>
      <Text>npx react-native run-android</Text>
      <Text>npx react-native start</Text>

      <Text>Try editing me! 🎉</Text>
      <Text>My Android version is: {Platform.Version}</Text>
      <StatusBar style="auto" />
      <Cat name="Munkustrap" />
      <Cat name="Spot" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
 */
