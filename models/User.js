const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new mongoose.Schema({
  name: String,
  lastname: String,
  picture: String, // VER Buffer para upload?
  age: Number,
  type: String, //  Cliente / Propietario
  card: Number,
  cellphone: {
    type: Number,
    required: true,
  },
  username: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  creation_date: {
    type: Date,
  },
  hidden: {
    type: Boolean,
    default: false,
  },
  rating: {
    type: Number,
    min: 1,
    max: 10,
  },
  acknowledgments: Schema.Types.Mixed, //  'reconocimientos' de los clientes
  badges: [String], // 'logros' que otorga la app
});

module.exports = mongoose.model("User", userSchema);
